﻿namespace DepositBanking.Repository.DepositBankingEntityModels
{
    using DepositBanking.Repository.IdentityModels;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    
        public class DepositBankingEntityContext : IdentityDbContext<ApplicationUser>
    {
        public DepositBankingEntityContext()
            : base("name=DepositBankingDb")
        {
        }

            public virtual DbSet<Bank> Banks { get; set; }
            public virtual DbSet<Depositor> Depositors { get; set; }
            public virtual DbSet<Investment> Investments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
        public static DepositBankingEntityContext Create()
        {
            return new DepositBankingEntityContext();
        }

    }
 

}