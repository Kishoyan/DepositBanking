﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DepositBanking.Repository.IdentityModels
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public DateTime Birthday { get; set; }
        public string Gender { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? DestroyDate { get; set; }

        public ApplicationUser()
        {
        }
    }
}
