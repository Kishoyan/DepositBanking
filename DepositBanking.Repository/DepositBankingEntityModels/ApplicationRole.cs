﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;

namespace DepositBanking.Repository.IdentityModels
{
    [NotMapped]
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() { }

    }
}
