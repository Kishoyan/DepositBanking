﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DepositBanking.Repository.DepositBankingEntityModels
{
    public class Bank
    {
        public Bank()
        {
            Investments = new List<Investment>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string OrganizationHead { get; set; }

        [Required]
        [StringLength(200)]
        public string Address { get; set; }

        public int? Branch { get; set; }

        [Required]
        [StringLength(100)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(20)]
        public string SWIFT { get; set; }

        [Required]
        [StringLength(20)]
        public string TPIN { get; set; }

        [StringLength(20)]
        public string Logo { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? DestroyDate { get; set; }

        public virtual ICollection<Investment> Investments { get; set; }

        
    }
}
