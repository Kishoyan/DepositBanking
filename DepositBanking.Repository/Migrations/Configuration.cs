namespace DepositBanking.Repository.Migrations
{
    using DepositBanking.Repository.DepositBankingEntityModels;
    using DepositBanking.Repository.IdentityModels;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<DepositBankingEntityContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "DepositBanking.Repository.DepositBankingEntityModels.DepositBankingEntityContext";
        }

        protected override void Seed(DepositBankingEntityContext context)
        {
            Bank b1 = new Bank
            {
                Name = "ACBA-CREDIT-AGRICOL BANK CJSC",
                OrganizationHead = "Hakob Andreasyan",
                Address = "Arami str. building No 82-84, territory No 87,89,99,100, Yerevan",
                Branch = 58,
                Phone = "565 858",
                Fax = "54-34-85",
                Email = "acba@acba.am",
                SWIFT = "AGCAAM22",
                TPIN = "02520007",
                Logo = "acba",
                CreationDate = DateTime.Now
            };
            Bank b2 = new Bank
            {
                Name = "Ameriabank CJSC",
                OrganizationHead = "Artak Hanesyan",
                Address = "Grigor Lusavorich str.9, Yerevan",
                Branch = 13,
                Phone = "58-99-06, 56-11-11, 56-43-19",
                Fax = "56-59-58",
                Email = "info@bank.am",
                SWIFT = "ARMIAM22",
                TPIN = "02502212",
                Logo = "ameria",
                CreationDate = DateTime.Now
            };
            Bank b3 = new Bank
            {
                Name = "Anelik Bank CJCS",
                OrganizationHead = "Nerses Karamanukyan",
                Address = "Vardanants str. 13, Yerevan",
                Branch = 13,
                Phone = "593 300, 593 301",
                Fax = "22-65-81",
                Email = "anelik@anelik.am",
                SWIFT = "ANIKAM22",
                TPIN = "00005409",
                Logo = "anelik",
                CreationDate = DateTime.Now
            };
            Bank b4 = new Bank
            {
                Name = "HSBC Bank Armenia",
                OrganizationHead = "Paul Edgar",
                Address = "Teryan str. 66,Yerevan",
                Branch = 9,
                Phone = "(3741) 515000, 515000",
                Fax = "(3741)515001",
                Email = "hsbc.armenia@hsbc.com",
                SWIFT = "MIDLAM22",
                TPIN = "02514855",
                Logo = "hsbc",
                CreationDate = DateTime.Now
            };
            Bank b5 = new Bank
            {
                Name = "Converse Bank Corp",
                OrganizationHead = "Arthur Hakobyan",
                Address = "V.Sargisyan str. 26/1, Yerevan",
                Branch = 34,
                Phone = "511200, 511205, 511211",
                Fax = "54 09 20",
                Email = "post@conversebank.am",
                SWIFT = "COVBAM22",
                TPIN = "00000291",
                Logo = "converse",
                CreationDate = DateTime.Now
            };
            Bank b6 = new Bank
            {
                Name = "ARMBUSINESSBANK",
                OrganizationHead = "Arsen Mikaelyan",
                Address = "Nalbandyan str. 48, Yerevan",
                Branch = 51,
                Phone = "592 010, 592 015",
                Fax = " 54-58-35",
                Email = "ibank@dolphin.am",
                SWIFT = "ARMNAM22",
                TPIN = "01500362",
                Logo = "abb",
                CreationDate = DateTime.Now
            };
            context.Banks.Add(b1);
            context.Banks.Add(b2);
            context.Banks.Add(b3);
            context.Banks.Add(b4);
            context.Banks.Add(b5);
            context.Banks.Add(b6);

            Depositor d1 = new Depositor
            {
                Name = "George",
                Surname = "L. Johnston",
                Address = "2232 Del Dew Drive Gaithersburg, MD 20877",
                Phone = "301-840-4143",
                Email = "GeorgeLJohnston@dayrep.com",
                Birthday = new DateTime(1982, 6, 13),
                Gender = "Male",
                CreationDate = DateTime.Now
            };
            Depositor d2 = new Depositor
            {
                Name = "Eric",
                Surname = "T. Black",
                Address = "2255 Rafe Lane Durant, MS 39063",
                Phone = "3662-653-3946",
                Email = "EricTBlack@jourrapide.com",
                Birthday = new DateTime(1963, 3, 15),
                Gender = "Male",
                CreationDate = DateTime.Now
            };
            Depositor d3 = new Depositor
            {
                Name = "Agnes",
                Surname = "T. Smith",
                Address = "4573 Blackwell Street Wasilla, AK 99654",
                Phone = "907-354-1371",
                Email = "AgnesTSmith@teleworm.us",
                Birthday = new DateTime(1976, 5, 5),
                Gender = "Female",
                CreationDate = DateTime.Now
            };
           
            context.Depositors.Add(d1);
            context.Depositors.Add(d2);
            context.Depositors.Add(d3);

            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            var role1 = new IdentityRole { Name = "admin" };
            var role2 = new IdentityRole { Name = "user" };

            roleManager.Create(role1);
            roleManager.Create(role2);

            var admin = new ApplicationUser
            {
                Name = "Jerry",
                Surname = "D. Newbold",
                Address = "1254 Mesa Drive Las Vegas, NV 89101",
                PhoneNumber = "702-348-9021",
                Email = "JerryDNewbold@teleworm.us",
                UserName= "JerryDNewbold@teleworm.us",
                Birthday = new DateTime(1965, 8, 1),
                Gender = "Male",
                CreationDate = DateTime.Now
            };
            string password = "some_password11";
            var result = userManager.Create(admin, password);

            if (result.Succeeded)
            {
                userManager.AddToRole(admin.Id, role1.Name);
            }
        }
    }
}
