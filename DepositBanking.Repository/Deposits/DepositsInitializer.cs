﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace DepositBanking.Repository.Deposits
{
    public static class DepositsInitializer
    {
        static List<DepositInfo> _deposits = new List<DepositInfo>();

        public static ReadOnlyCollection<DepositInfo> Deposits
        {
            get
            {
                return _deposits.AsReadOnly();
            }
        }
        static DepositsInitializer()
        {
            CurrencyInfo c1 = new CurrencyInfo("USD", 200);
            CurrencyInfo c2 = new CurrencyInfo("AMD", 50000);
            CurrencyInfo c3 = new CurrencyInfo("EUR", 200);
            CurrencyInfo c4 = new CurrencyInfo("RUR", 5000);

            List<CurrencyInfo> _currencies = new List<CurrencyInfo>() { c1, c2, c3, c4 };

            DepositInfo d1 = new DepositInfo("Accumulative", 13, 6, 36, DateTime.Now, DateTime.Now.AddMonths(6), _currencies);
            DepositInfo d2 = new DepositInfo("FamilyTyme", 12, 3, 24, DateTime.Now, DateTime.Now.AddMonths(3), _currencies);
            DepositInfo d3 = new DepositInfo("Saving", 9, 1, 12, DateTime.Now, DateTime.Now.AddMonths(1), _currencies);

            _deposits = new List<DepositInfo>() { d1, d2, d3 };

        }
    }
}