﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DepositBanking.Repository.Deposits
{
    public class CurrencyInfo
    {
        public CurrencyInfo()
        {

        }
        public string Name { get; }
        public double MinAmount { get; }

        public CurrencyInfo(string name, double minAmount)
        {
            Name = name;
            MinAmount = minAmount;
        }
    }
}