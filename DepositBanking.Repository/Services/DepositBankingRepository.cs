﻿using DepositBanking.Repository.DepositBankingEntityModels;
using DepositBanking.Repository.Deposits;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DepositBanking.Repositories
{
    public class DepositBankingRepository : IDepositBankingRepository, IDisposable
    {
        private DepositBankingEntityContext _context;

        public DepositBankingRepository()
        {
            _context = new DepositBankingEntityContext();
        }
          
        public IEnumerable<T> GetAll<T>() where T : class
        {
            return _context.Set<T>().ToList();
        }

        public T GetById<T>(int id) where T : class
        {
            return _context.Set<T>().Find(id);
        }
        
        public bool Create<T>(T item) where T : class
        {
            if (item == null)
                return false;
            try
            {
                _context.Set<T>().Add(item);
                return _context.SaveChanges() >= 0;
            }
            catch
            {
                return false;
            }
        }

        public void Update<T>(T item) where T : class
        {            
            _context.Set<T>().AddOrUpdate(item);
            //_context.Entry<T>(item).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete<T>(int id) where T : class
        {
            T item = _context.Set<T>().Find(id);
            _context.Set<T>().Remove(item);
            _context.SaveChanges();
        }


        public Depositor GetDepositorByEmail(string email)
        {
            return _context.Set<Depositor>().FirstOrDefault(d=>d.Email==email && d.DestroyDate==null);
        }

        public DepositInfo GetDepositInfoByName(string type)
        {
            return DepositsInitializer.Deposits.Where(c => c.Type == type).FirstOrDefault();
        }

        public IEnumerable<Bank> GetAllActiveBanks()
        {
            return _context.Set<Bank>().Where(b=>b.DestroyDate==null).ToList();
        }

        public IEnumerable<Depositor> GetAllActiveDepositors()
        {
            return _context.Set<Depositor>().Where(d => d.DestroyDate == null).ToList();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        
        //Is used in second version of search functionality

        //public IEnumerable<Investment> Search(string queryString)
        //{
        //    return _context.Investments.SqlQuery(queryString).ToList();

        //}
    }

    }