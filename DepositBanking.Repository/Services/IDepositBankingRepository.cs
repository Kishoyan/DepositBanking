﻿using DepositBanking.Repository.DepositBankingEntityModels;
using DepositBanking.Repository.Deposits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DepositBanking.Repositories
{
    public interface IDepositBankingRepository : IDisposable
    {
        IEnumerable<T> GetAll<T>() where T : class;
        T GetById<T>(int id) where T:class;
        bool Create<T>(T item) where T : class;
        void Update<T>(T item) where T : class;
        void Delete<T>(int id) where T : class;

               
        Depositor GetDepositorByEmail(string email);
        IEnumerable<Bank> GetAllActiveBanks();
        IEnumerable<Depositor> GetAllActiveDepositors();
        DepositInfo GetDepositInfoByName(string type);


        //Is used in second version of search functioality
        //IEnumerable<Investment> Search(string queryString);


    }
}
