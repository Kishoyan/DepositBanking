﻿using Microsoft.Owin;
using Owin;
using DepositBanking.Repository.IdentityModels;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using DepositBanking.Repository.DepositBankingEntityModels;

[assembly: OwinStartup(typeof(Volo.Luiza_Kishoyan.DepositBanking.App_Start.Startup))]

namespace Volo.Luiza_Kishoyan.DepositBanking.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<DepositBankingEntityContext>(DepositBankingEntityContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });
        }
    }
}