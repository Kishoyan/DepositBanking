﻿using System.Web;
using System.Web.Mvc;

namespace Volo.Luiza_Kishoyan.DepositBanking
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
