﻿using DepositBanking.Repository.DepositBankingEntityModels;
using DepositBanking.Repository.IdentityModels;
using DepositBanking.Repository.Migrations;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;
using System.Diagnostics;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Volo.Luiza_Kishoyan.DepositBanking
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //Most be closed after first initialization.
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DepositBankingEntityContext, Configuration>());

            //Database.SetInitializer(new DepositBankingDBInitializer());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
