﻿using DepositBanking.Repository.DepositBankingEntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Volo.Luiza_Kishoyan.DepositBanking.ViewModels.AccountViewModels
{
    public class UserProfileViewModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Birthday { get; set; }
        public string Gender { get; set; }
        public string UserName { get; set; }
        public DateTime CreationDate { get; set; }

        public ICollection<Investment> Investments { get; set; }
    }
}