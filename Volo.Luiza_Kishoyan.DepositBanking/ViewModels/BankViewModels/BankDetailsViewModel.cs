﻿using DepositBanking.Repository.DepositBankingEntityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Volo.Luiza_Kishoyan.DepositBanking.ViewModels.BankViewModels
{
    public class BankDetailsViewModel
    {

      public BankDetailsViewModel()
        {
            Investments = new HashSet<Investment>();
            Depositors = new HashSet<Depositor>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string OrganizationHead { get; set; }

        public string Address { get; set; }

        public int? Branch { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string SWIFT { get; set; }

        public string TPIN { get; set; }

        public string Logo { get; set; }

        public DateTime CreationDate { get; set; }

        public string State { get; set; }

        public ICollection<Investment> Investments { get; set; }
     
        public ICollection<Depositor> Depositors { get; set; }
    }
}