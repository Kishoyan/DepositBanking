﻿function FillDepositType() {
    var depositName = $('#Type').val();

    $.ajax({
        url: '/Base/FillDepositType',
        type: "GET",
        dataType: "JSON",
        data: { Name: depositName},
        success: function (deposit) {
            $("#Currency").html(""); 
            $("#Rate").val(deposit.Rate);
            $("#Duration").val(deposit.MinDuration);
            $("#Duration").attr("min",deposit.MinDuration);
            $("#Duration").attr("max",deposit.MaxDuration);
            $("#StartDate").val(deposit.MinStartDate);
            $("#StartDate").attr("min",deposit.MinStartDate);
            $("#EndDate").val(deposit.MinEndDate);
            $.each(deposit.Currencies, function (i, currency) {
                $("#Currency").append(
                    $('<option></option>').val(currency.Name).html(currency.Name));
            });
            var Name=$("#Currency").val();
            var result = $.grep(deposit.Currencies, function(e){ return e.Name === Name; });
            $("#Amount").val(result[0].MinAmount);
            $("#Amount").attr("min",result[0].MinAmount);

             $("#Currency").change(function(){
                $("#FinalAmount").val("");
                var Name=$("#Currency").val();
                var result = $.grep(deposit.Currencies, function(e){ return e.Name === Name; });
                $("#Amount").val(result[0].MinAmount);  
                $("#Amount").attr("min",result[0].MinAmount);
                });
     }
    });

  }
