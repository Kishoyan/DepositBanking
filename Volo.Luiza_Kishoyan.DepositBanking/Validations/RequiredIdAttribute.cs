﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Volo.Luiza_Kishoyan.DepositBanking.Validations
{
    public class RequiredIdAttribute:ValidationAttribute
    {
        public RequiredIdAttribute()
        {

        }
        public override bool IsValid(object value)
        {
            var id = (int)value;
            if (id > 0)
            {
                return true;
            }
            return false;
        }
    }
}