﻿using AutoMapper;
using DepositBanking.Repository.Deposits;
using DepositBanking.Repository.DepositBankingEntityModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.InvestmentViewModels;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.SearchViewModels;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class SearchController : BaseController
    {
        public ActionResult Search()
        {
            SearchViewModel searchModel = new SearchViewModel();
            searchModel.Banks = BanksDropdownListCreater(depositBankingRepository.GetAllActiveBanks());
            searchModel.Depositors = DepositorsDropdownListCreater(depositBankingRepository.GetAllActiveDepositors());
            searchModel.Deposits = DepositsDropdownListCreater(DepositsInitializer.Deposits);
            
            searchModel.Bank = searchModel.Banks.FirstOrDefault().Name;
            searchModel.Depositor = searchModel.Depositors.FirstOrDefault().FullName;
            searchModel.Deposit = searchModel.Deposits.FirstOrDefault().Type;
            
            return View(searchModel);
        }

        [HttpPost]
        public ActionResult SearchResults(SearchViewModel searchModel)
        {
            var Results = GetSearchResults(searchModel);

            return PartialView(Results);
        }


        public List<SearchResultsViewModel> GetSearchResults(SearchViewModel searchModel)
        {
            Mapper.Initialize(b => b.CreateMap<Investment, InvestmentIndexViewModel>()
                .ForMember("Bank", opt => opt.MapFrom(n => n.Bank.Name))
                .ForMember("Depositor", opt => opt.MapFrom(n => n.Depositor.Name + " " + n.Depositor.Surname)));
            var investments = Mapper.Map<IEnumerable<Investment>, IEnumerable<InvestmentIndexViewModel>>(depositBankingRepository.GetAll<Investment>()).AsQueryable();

            List<SearchResultsViewModel> results = new List<SearchResultsViewModel>();
            if (searchModel != null)
            {
                if (string.IsNullOrEmpty(searchModel.Bank) && string.IsNullOrEmpty(searchModel.Depositor) && string.IsNullOrEmpty(searchModel.Deposit) &&
                        !searchModel.AmountFrom.HasValue && !searchModel.AmountTo.HasValue && !searchModel.StartDateFrom.HasValue && !searchModel.StartDateTo.HasValue)
                {
                    return results;
                }
                if (!string.IsNullOrEmpty(searchModel.Bank))
                    investments = investments.Where(x => x.Bank.Contains(searchModel.Bank));
                if (!string.IsNullOrEmpty(searchModel.Depositor))
                    investments = investments.Where(x => x.Depositor.Contains(searchModel.Depositor));
                if (!string.IsNullOrEmpty(searchModel.Deposit))
                    investments = investments.Where(x => x.Type.Contains(searchModel.Deposit));
                if (searchModel.AmountFrom.HasValue)
                    investments = investments.Where(x => x.Amount >= searchModel.AmountFrom);
                if (searchModel.AmountTo.HasValue)
                    investments = investments.Where(x => x.Amount <= searchModel.AmountTo);
                if (searchModel.StartDateFrom.HasValue)
                    investments = investments.Where(x => x.StartDate >= searchModel.StartDateFrom);
                if (searchModel.StartDateTo.HasValue)
                    investments = investments.Where(x => x.StartDate <= searchModel.StartDateTo);

                foreach (var item in investments)
                {
                    var res = new SearchResultsViewModel()
                    {
                        DepositorName = item.Depositor,
                        BankName = item.Bank,
                        DepositType = item.Type
                    };

                    results.Add(res);
                }
            }
            return results;
        }

        //Second version of search functionality

        //public IEnumerable<SearchResultsViewModel> GetSearchResults(SearchViewModel searchModel)
        //{
        //    List<SearchResultsViewModel> results = new List<SearchResultsViewModel>();
        //    var queryString = new StringBuilder("Select * From Investments Where");
        //    if (searchModel != null)
        //    {
        //        if (searchModel.BankId == 0 && searchModel.BankId == 0 && string.IsNullOrEmpty(searchModel.Deposit) &&
        //                !searchModel.AmountFrom.HasValue && !searchModel.AmountTo.HasValue && !searchModel.StartDateFrom.HasValue && !searchModel.StartDateTo.HasValue)
        //        {
        //            return results;
        //        }

        //        if (searchModel.BankId != 0)
        //            queryString.Append(" BankId = ").Append(searchModel.BankId);

        //        if (searchModel.DepositorId !=0)
        //        {
        //            if (searchModel.BankId == 0)
        //                queryString.Append(" DepositorId = ").Append(searchModel.DepositorId);
        //            else queryString.Append(" And DepositorId = ").Append(searchModel.DepositorId);
        //        }                  

        //        if (!string.IsNullOrEmpty(searchModel.Deposit))
        //        {
        //            if (searchModel.BankId == 0 && searchModel.DepositorId ==0)
        //                queryString.Append(" Type = '").Append(searchModel.Deposit).Append("'");
        //            else queryString.Append(" And Type = '").Append(searchModel.Deposit).Append("'");
        //        }

        //        if (searchModel.AmountFrom.HasValue)
        //        {
        //            if (searchModel.BankId == 0 && searchModel.DepositorId ==0 && string.IsNullOrEmpty(searchModel.Deposit))
        //                queryString.Append(" Amount >= ").Append(searchModel.AmountFrom);
        //            else queryString.Append(" And Amount >= ").Append(searchModel.AmountFrom);

        //        }

        //        if (searchModel.AmountTo.HasValue)
        //        {
        //            if (searchModel.BankId == 0 && searchModel.DepositorId ==0 && string.IsNullOrEmpty(searchModel.Deposit) && !searchModel.AmountFrom.HasValue)
        //                queryString.Append(" Amount <= ").Append(searchModel.AmountTo);
        //            else queryString.Append(" And Amount <= ").Append(searchModel.AmountTo);
        //        }

        //        if (searchModel.StartDateFrom.HasValue)
        //        {
        //            if (searchModel.BankId == 0 && searchModel.DepositorId ==0 && string.IsNullOrEmpty(searchModel.Deposit) && !searchModel.AmountFrom.HasValue && !searchModel.AmountTo.HasValue)
        //                queryString.Append(" StartDate >= ").Append(searchModel.StartDateFrom));
        //            else queryString.Append(" And StartDate >= ").Append(searchModel.StartDateFrom);
        //        }                    

        //        if (searchModel.StartDateTo.HasValue)
        //        {
        //            if (searchModel.BankId == 0 && searchModel.DepositorId ==0 && string.IsNullOrEmpty(searchModel.Deposit) && !searchModel.AmountFrom.HasValue && !searchModel.AmountTo.HasValue && !searchModel.StartDateFrom.HasValue)
        //                queryString.Append(" StartDate <= ").Append(searchModel.StartDateTo);
        //            else queryString.Append(" And StartDate <= ").Append(searchModel.StartDateTo);
        //        }

        //    }                               
        //        Mapper.Initialize(b => b.CreateMap<Investment, InvestmentIndexViewModel>()
        //        .ForMember("Bank", opt => opt.MapFrom(n => n.Bank.Name))
        //        .ForMember("Depositor", opt => opt.MapFrom(n => n.Depositor.Name + " " + n.Depositor.Surname)));
        //        var investments = Mapper.Map<IEnumerable<Investment>, IEnumerable<InvestmentIndexViewModel>>(depositBankingRepository.Search(queryString.ToString()));
        //        foreach (var item in investments)
        //        {
        //            var res = new SearchResultsViewModel()
        //            {
        //                DepositorName = item.Depositor,
        //                BankName = item.Bank,
        //                DepositType = item.Type
        //            };

        //            results.Add(res);
        //        }

        //        return results;
        //}
        
    }
}