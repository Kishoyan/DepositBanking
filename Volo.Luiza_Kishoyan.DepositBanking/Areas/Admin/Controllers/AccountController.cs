﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using DepositBanking.Repository.IdentityModels;
using Volo.Luiza_Kishoyan.DepositBanking.ViewModels.AccountViewModels;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Web.Security;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.Controllers
{
    public class AccountController : BaseController
    {
        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            ViewBag.UserName = "JerryDNewbold@teleworm.us";
            ViewBag.Password = "some_password11";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(AccountLoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await UserManager.FindAsync(model.Email, model.Password);

                if (user == null)
                {
                    ModelState.AddModelError("", "Incorrect Login and Password");
                }
                else
                {
                    Session["UserID"] = user.Id.ToString();
                    Session["UserName"] = user.UserName.ToString();

                    ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = false
                    }, claim);
                    FormsAuthentication.SetAuthCookie("CookieValue", false);
                    if (String.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("Index", "Banks");
                    return Redirect(returnUrl);
                }
            }
            ViewBag.returnUrl = returnUrl;
            ViewBag.UserName = "JerryDNewbold@teleworm.us";
            ViewBag.Password = "some_password11";
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            Session["UserName"] = null;
            return RedirectToAction("Index","Home",new { area=""});
        }

    }
}