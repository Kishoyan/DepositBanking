﻿using DepositBanking.Repository.DepositBankingEntityModels;
using System;
using System.Collections.Generic;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels
{
    public class DepositorDetailsViewModel
    {
        public DepositorDetailsViewModel()
        {
            Investments = new HashSet<Investment>();
            Banks = new HashSet<Bank>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public DateTime Birthday { get; set; }

        public string Gender { get; set; }

        public DateTime CreationDate { get; set; }

        public string State { get; set; }

        
        public ICollection<Investment> Investments { get; set; }

        public ICollection<Bank> Banks { get; set; }
    }
}