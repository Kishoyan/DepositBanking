﻿using DepositBanking.Repository.DepositBankingEntityModels;
using DepositBanking.Repository.Deposits;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.BankViewModels;
using Volo.Luiza_Kishoyan.DepositBanking.Validations;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels
{
    public class DepositorInvestViewModel
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "Select the Type")]
        public string Type { get; set; }

        public double Rate { get; set; }

        public double Amount { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [Required]
        public string Currency { get; set; }

        public int Duration { get; set; }

        [RequiredId(ErrorMessage = "Select the Bank")]
        public int BankId { get; set; }

        [Required(ErrorMessage = "Select the Depositor")]
        public int? DepositorId { get; set; }

        public string BankState { get; set; }

        public string DepositorState { get; set; }

        public DateTime CreationDate { get; set; }
        
        public Depositor Depositor { get; set; }

        public IEnumerable<BankIndexViewModel> Banks { get; set; }
        
        public IEnumerable<DepositInfo> Deposits { get; set; }

        public IEnumerable<CurrencyInfo> Currencies { get; set; }
    }
}