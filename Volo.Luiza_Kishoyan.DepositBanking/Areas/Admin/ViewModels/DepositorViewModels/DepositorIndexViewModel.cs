﻿using System;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels
{
    public class DepositorIndexViewModel
    {
        public DepositorIndexViewModel() {}

        public int Id { get; set; }

        public string FullName { get; set; }

        public string Address { get; set; }
        
        public string Email { get; set; }

        public DateTime Birthday { get; set; }

    }
}