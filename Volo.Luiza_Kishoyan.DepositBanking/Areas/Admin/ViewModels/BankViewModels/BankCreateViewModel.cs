﻿using System.ComponentModel.DataAnnotations;


namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.BankViewModels
{
    public class BankCreateViewModel
    {
        
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string OrganizationHead { get; set; }

        [Required]
        [StringLength(200)]
        public string Address { get; set; }

        public int? Branch { get; set; }

        [Required]
        [StringLength(100)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(20)]
        public string SWIFT { get; set; }

        [Required]
        [StringLength(20)]
        public string TPIN { get; set; }

        [StringLength(20)]
        public string Logo { get; set; }
        
    }
}