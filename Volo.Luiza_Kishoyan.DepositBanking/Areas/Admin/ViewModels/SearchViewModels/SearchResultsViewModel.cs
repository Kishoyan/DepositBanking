﻿
namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.SearchViewModels
{
    public class SearchResultsViewModel
    {
        public string DepositorName { get; set; }
        public string DepositType { get; set; }
        public string BankName { get; set; }
    }
}