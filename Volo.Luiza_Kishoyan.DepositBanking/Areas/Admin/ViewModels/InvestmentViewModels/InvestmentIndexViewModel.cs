﻿using System;

namespace Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.InvestmentViewModels
{
    public class InvestmentIndexViewModel
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public double Rate { get; set; }

        public double Amount { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Currency { get; set; }

        public int Duration { get; set; }

        public double? FinalAmount { get; set; }

        public string Bank { get; set; }

        public string Depositor { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? DestroyDate { get; set; }
    }
}