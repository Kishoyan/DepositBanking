﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using DepositBanking.Repository.IdentityModels;
using Volo.Luiza_Kishoyan.DepositBanking.ViewModels.AccountViewModels;
using AutoMapper;
using Microsoft.Owin.Security;
using System.Security.Claims;
using Volo.Luiza_Kishoyan.DepositBanking.Areas.Admin.ViewModels.DepositorViewModels;
using System.Web.Security;
using System.Net;
using DepositBanking.Repository.DepositBankingEntityModels;
using DepositBanking.Repository.Deposits;
using System.Collections.Generic;

namespace Volo.Luiza_Kishoyan.DepositBanking.Controllers
{
    public class AccountController : Areas.Admin.Controllers.BaseController
    {
        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Register(AccountRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                Mapper.Initialize(b => b.CreateMap<AccountRegisterViewModel, ApplicationUser>()
                    .ForMember("UserName", opt => opt.MapFrom(n => n.Email))
                    .ForMember("PhoneNumber", opt => opt.MapFrom(n => n.Phone))
                    .ForMember("CreationDate", opt => opt.MapFrom(d => DateTime.Now)));

                ApplicationUser user = Mapper.Map<AccountRegisterViewModel, ApplicationUser>(model);
                
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
               
                if (result.Succeeded)
                {
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
             }
            return View(model);
        }

        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(AccountLoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await UserManager.FindAsync(model.Email, model.Password);

                if (user == null)
                {
                    ModelState.AddModelError("", "Incorrect Login and Password");
                }
                else
                {
                    Session["UserID"] = user.Id.ToString();
                    Session["UserName"] = user.UserName.ToString();
                    ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = false
                    }, claim);
                    FormsAuthentication.SetAuthCookie("CookieValue", false);
                    if (String.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("UserProfile", "Account", user);
                    return Redirect(returnUrl);
                }
            }
            ViewBag.returnUrl = returnUrl;
           
            return View(model);
        }

        public ActionResult UserProfile(ApplicationUser user)
        {
            if (user.Email == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser currentUser = UserManager.FindByEmail(user.Email);
            Mapper.Initialize(b => b.CreateMap<ApplicationUser, UserProfileViewModel>());
            UserProfileViewModel userProfile = Mapper.Map<ApplicationUser, UserProfileViewModel>(currentUser);

            var depositor = depositBankingRepository.GetDepositorByEmail(user.Email);

            if (depositor != null)
            {
                userProfile.Investments = depositor.Investments;
            }
                return View(userProfile);
        }

        public ActionResult Invest(string email)
        {
            if (email == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DepositorInvestViewModel investment = new DepositorInvestViewModel();

            var depositor = depositBankingRepository.GetDepositorByEmail(email);
         
            if (depositor == null)
            {
                ApplicationUser currentUser = UserManager.FindByEmail(email);

                Depositor newDepositor = new Depositor()
                {
                    Name = currentUser.Name,
                    Surname = currentUser.Surname,
                    Address = currentUser.Address,
                    Phone = currentUser.PhoneNumber,
                    Email = currentUser.Email,
                    Birthday = currentUser.Birthday,
                    Gender = currentUser.Gender,
                    CreationDate = DateTime.Now
                };
                depositBankingRepository.Create<Depositor>(newDepositor); 
                
                investment.Depositor = depositBankingRepository.GetDepositorByEmail(email);
                investment.DepositorId = investment.Depositor.Id;
            }
            else
            {
                investment.DepositorId = depositor.Id;
                investment.Depositor = depositor;
            }
            
            investment.Banks = BanksDropdownListCreater(depositBankingRepository.GetAllActiveBanks());
            investment.Deposits = DepositsDropdownListCreater(DepositsInitializer.Deposits);
            investment.Currencies = new List<CurrencyInfo>();

            return View(investment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Invest(DepositorInvestViewModel investment)
        {

            investment.Banks = BanksDropdownListCreater(depositBankingRepository.GetAllActiveBanks());
            investment.Deposits = DepositsDropdownListCreater(DepositsInitializer.Deposits);
            investment.Depositor = depositBankingRepository.GetById<Depositor>(investment.DepositorId.Value);
            if (investment.Type != null)
            {
                investment.Currencies = depositBankingRepository.GetDepositInfoByName(investment.Type).Currencies;
            }
            else
            {
                investment.Currencies = new List<CurrencyInfo>();
            }

            if (ModelState.IsValid)
            {
                ViewBag.ShowModal = true;
                Mapper.Initialize(b => b.CreateMap<DepositorInvestViewModel, Investment>()
                    .ForMember("CreationDate", opt => opt.MapFrom(d => DateTime.Now))
                    .ForMember("FinalAmount", opt => opt.MapFrom(d => d.Amount + d.Duration * ((d.Rate / 100) / 12) * d.Amount))
                    .ForMember("BankState", opt => opt.MapFrom(d => "Active"))
                    .ForMember("DepositorState", opt => opt.MapFrom(d => "Active")));
                Investment newInvestment = Mapper.Map<DepositorInvestViewModel, Investment>(investment);

                depositBankingRepository.Create<Investment>(newInvestment);

                return RedirectToAction("Index", "Home");
            }

            return View(investment);
        }
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            Session["UserName"] = null;
            return RedirectToAction("Index","Home");
        }

    }
}