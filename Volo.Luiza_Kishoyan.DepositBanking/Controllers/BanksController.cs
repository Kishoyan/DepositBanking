﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using DepositBanking.Repository.DepositBankingEntityModels;
using AutoMapper;
using Volo.Luiza_Kishoyan.DepositBanking.ViewModels.BankViewModels;

namespace Volo.Luiza_Kishoyan.DepositBanking.Controllers
{
    public class BanksController : Areas.Admin.Controllers.BaseController
    {
        public ActionResult Index()
        {
            Mapper.Initialize(b => b.CreateMap<Bank, BankIndexViewModel>());
            var banks = Mapper.Map<IEnumerable<Bank>, IEnumerable<BankIndexViewModel>>(depositBankingRepository.GetAllActiveBanks());
          
            return View(banks);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mapper.Initialize(b => b.CreateMap<Bank, BankDetailsViewModel>()
                .ForMember("State", opt => opt.MapFrom(n => n.DestroyDate==null ? "Active" : "Not Active")));
            BankDetailsViewModel bank = Mapper.Map<Bank, BankDetailsViewModel>(depositBankingRepository.GetById<Bank>(id.Value));
            
            if (bank == null)
            {
                return HttpNotFound();
            }

            return View(bank);
        }
      
    }
}
