﻿using AutoMapper;
using DepositBanking.Repository.Deposits;
using System.Collections.Generic;
using System.Web.Mvc;
using Volo.Luiza_Kishoyan.DepositBanking.ViewModels.HomeViewModels;

namespace Volo.Luiza_Kishoyan.DepositBanking.Controllers
{
    public class HomeController : Areas.Admin.Controllers.BaseController
    {
        public ActionResult Index()
        {
           return View();
        }

        [HttpGet]
        public PartialViewResult Calculator(string type)
        {            
            CalculatorViewModel model = new CalculatorViewModel();
            model.Deposits = DepositsInitializer.Deposits;
            model.Currencies = new List<CurrencyInfo>();
           
            return PartialView(model);
        }

        public ActionResult About()
        {            
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}